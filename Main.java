package com.IN3;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import java.io.IOException;
import java.net.URI;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Main class.
 *
 */
public class Main {
    // Base URI the Grizzly HTTP server will listen on
    public static final String BASE_URI = "http://0.0.0.0:9002";
    public static Connection connection;

    /**
     * Starts Grizzly HTTP server exposing JAX-RS resources defined in this application.
     * @return Grizzly HTTP server.
     */
    public static HttpServer startServer() {
        // create a resource config that scans for JAX-RS resources and providers
        // in com.IN3 package
        final ResourceConfig rc = new ResourceConfig().packages("com.IN3");
        rc.register("javax.ws.rs.container.ContainerResponseFilter");
        rc.register("com.IN3.CrossDomainFilter");

        /*Start MySQL Service */

        String dbUrl = "jdbc:mysql://127.0.0.1:8889/ChildSafety?autoReconnect=true";
        String dbClass = "com.mysql.jdbc.Driver";
        String username = "web";
        String password = "password";
        try {
            Class.forName(dbClass);
            connection = DriverManager.getConnection(dbUrl,
            username, password);
            } catch (ClassNotFoundException e) {
            e.printStackTrace();
            } catch (SQLException e) {
            e.printStackTrace();
        }

        // create and start a new instance of grizzly http server
        // exposing the Jersey application at BASE_URI
        return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
    }

    /**
     * Main method.
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        final HttpServer server = startServer();
        System.out.println(String.format("Jersey app started with WADL available at "
                + "%sapplication.wadl\nHit enter to stop it...", BASE_URI));
        System.in.read();
        server.stop();
    }
}
