create table Users(UserId number(10,0), UserName Varchar(30), Password Varchar(30)) ;
create table Roles(RoleId number(10,0), RoleName Varchar(30), EncryptionKey Varchar(30)) ;
create table UserRoles(UserId number(10,0), RoleId number(10,0)) ;
create table Privileges(PrivId number(10,0), PrivName Varchar(30)); 
create table RolePrivileges(RoleId number(10,0), TableName Varchar(30), PrivId number(10,0)); 